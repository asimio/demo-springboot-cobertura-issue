# README #

Accompanying source code for blog entry at http://tech.asimio.net/2017/07/25/Fixing-LoggerFactory-not-a-Logback-LoggerContext-but-Logback-is-on-the-classpath-SpringBoot-Cobertura-Error.html

### Requirements ###

* Java 8
* Maven 3.3.x

### Building from command line ###

```
mvn clean cobertura:cobertura
```

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero