package com.asimio.cobertura.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCoberturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoCoberturaApplication.class, args);
	}
}
